function countLetter(letter, sentence) {
  count = 0;
  // Check first whether the letter is a single character.
  x = letter.length === 1;
  // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
  if (x === true) {
    sentence.split("").forEach((eachLetters) => {
      if (eachLetters === letter) count++;
    });
  }
  // If letter is invalid, return undefined.
  else {
    return undefined;
  }

  return count;
}

function isIsogram(text) {
  // An isogram is a word where there are no repeating letters.
  // The function should disregard text casing before doing anything else.
  x = text.toLowerCase();
  y = array = [];
  z = 0;
  // If the function finds a repeating letter, return false. Otherwise, return true.
  text.split("").forEach((eachLetters) => {
    array.push(eachLetters);
  });

  for (i = 0; i < text.length; i++) {
    for (o = 0; o < text.length; o++) {
      if (y[i] === y[o]) {
        count++;
      }
    }
  }

  if (count > 0) {
    return false;
  } else {
    return true;
  }
}

function purchase(age, price) {
  // Return undefined for people aged below 13.
  if (age < 13) {
    return undefined;
  }
  // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
  else if ((age >= 13 && age <= 21) || age >= 65) {
    x = price - price * 0.2;
    y = Math.round(x);
    z = String(y);
    return z;
  }
  // Return the rounded off price for people aged 22 to 64.
  else if (age >= 22 && age <= 64) {
    x = Math.round(price);
    y = String(x);
    return z;
  }
  // The returned value should be a string.
}

function findHotCategories(items) {
  console.log("test");
  // Find categories that has no more stocks.
  // The hot categories must be unique; no repeating categories.
  // The passed items array from the test are the following:
  let x = [];
  const y = items.filter((item) => item.stocks === 0);
  y.map((item) => {
    const z = x.findIndex((find) => item.category == find);

    if (z === -1) {
      x.push(item.category);
    }
  });

  return x;
  // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
  // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
  // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
  // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
  // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
  // The expected output after processing the items array is ['toiletries', 'gadgets'].
}

function findFlyingVoters(candidateA, candidateB) {
  // Find voters who voted for both candidate A and candidate B.
  // The passed values from the test are the following:
  // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
  // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
  // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

  let x = candidateA.filter((y) => candidateB.includes(y));

  return x;
}

module.exports = {
  countLetter,
  isIsogram,
  purchase,
  findHotCategories,
  findFlyingVoters,
};
